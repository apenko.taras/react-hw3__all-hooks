import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from "../pages/Home";
import Favorite from "../pages/Favorite";
import Cart from "../pages/Cart";

const AppRoutes = () => {
    return (
        <Switch>
            <Route path={'/favorite'} component={Favorite}/>
            <Route path={'/cart'} component={Cart}/>
            <Route path={'/'} component={Home}/>
        </Switch>
    );
};

export default AppRoutes;