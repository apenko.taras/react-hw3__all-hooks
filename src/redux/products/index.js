import productsReducer from './reducers.js';

export {default as productsTypes} from './types'
export {default as productsActions} from './actions'

export default productsReducer