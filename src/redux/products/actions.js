import {productsTypes} from "./index";

const getAllProducts = () => ({
    type: productsTypes.PRODUCTS_GET
})

const setAllProducts = (allProducts) => ({
    type: productsTypes.PRODUCTS_SET,
    payload: allProducts
})


export default {
    getAllProducts,
    setAllProducts
}