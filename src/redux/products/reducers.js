import {productsTypes} from "./index";

export default (state = [], action) => {
    switch (action.type) {
        case productsTypes.PRODUCTS_SET:
            return action.payload
        case productsTypes.PRODUCTS_ERROR:
            throw new Error("Something wrong")
        default:
            return state
    }
}