import React from 'react';
import './Btn.scss'

const Btn = ({backgroundColor, text, onClick}) => {

    return (
        <button
            className='btnMod'
            onClick={onClick}
            style={{backgroundColor: backgroundColor}}
        >{text}</button>
    )

}

export default Btn