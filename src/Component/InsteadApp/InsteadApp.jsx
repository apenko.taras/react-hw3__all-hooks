import React, {useEffect, useState} from 'react';
import '../../App.css';
import Modal from "../Modal/Modal";
import ProductList from "../ProductList/ProductList";
import {useDispatch, useSelector} from "react-redux";
import {productsActions} from "../../redux/products";


function InsteadApp(props) {

    const [cart, setCart] = useState({});
    const [favorite, setFavorite] = useState({});
    const [carToCart, setCarToCart] = useState('');
    const [displayModalId, setDisplayModalId] = useState('');
    const [buttons, setButtons] = useState({
        btnSubmit: {
            backgroundColor: 'green',
            text: 'Submit',
            onClick: () => {
                console.log('submit')
            }
        },
        btnCancel: {
            backgroundColor: '#CCCC00',
            text: 'Cancel',
            onClick: () => {
                onCancel()
            }
        }
    });

    const [modals, setModal] = useState({
        firstMod: {
            header: 'Add to cart',
            closeButton: true,
            text: 'Do you want to add it to cart?',
        },
        secondMod: {
            header: 'Remove from cart',
            closeButton: true,
            text: 'Are you sure to remove item from cart?',
        }
    });
    const [items, setItems] = useState([]);

    const [products, setProducts] = useState([])

    // const dispatch = useDispatch()

    useEffect(() => {
        const localStorageRef = localStorage.getItem(JSON.stringify("cart"))
        const localStorageFavor = localStorage.getItem(JSON.stringify("favorite"))
        if (localStorageRef) {
            setCart(JSON.parse(localStorageRef))
        }

        if (localStorageFavor) {
            setFavorite(JSON.parse(localStorageFavor))
        }

        fetch('/products.json')
            .then(response => response.json())
            .then(data => {setProducts(data);
            })
            .catch(err => console.error((err)));
    }, []);

    useEffect(() => {
        localStorage.setItem(JSON.stringify("cart"), JSON.stringify(cart))
    },[cart])

    useEffect(() => {
        localStorage.setItem(JSON.stringify("favorite"), JSON.stringify(favorite))
    },[favorite])

    // componentDidUpdate(prevProps, prevState, snapshot)
    // {
    //     localStorage.setItem(JSON.stringify("cart"), JSON.stringify(cart))
    //     localStorage.setItem(JSON.stringify("favorite"), JSON.stringify(favorite))
    // }

    const addToCart = (displayModalId) => {
        const key = carToCart
        const newcart = {...cart}
        if (displayModalId === 'first') {
            newcart[key] = newcart[key] + 1 || 1;
        } else if (displayModalId === 'second') {
            newcart[key] = 0;
        }
        setCart(newcart);
        setCarToCart('')
        setDisplayModalId('')
    }

    const addToFavorite = (index) => {
        const newfavorite = {...favorite}
        newfavorite[index] = !favorite[index];

        setFavorite(newfavorite)
    }

    const onCancel = () => {
        setDisplayModalId('')
    }

    const setActive = () => {
        setDisplayModalId('')
    }

    const showModal = (mod, articles) => {
        if (mod === 'first') {
            setCarToCart(articles)
        } else if (mod === 'second') {
            setCarToCart(articles)
        }
        setDisplayModalId(mod)
    }


    // render() {

    if (!products) {
        return <p>loading</p>
    }

    const {path} = props

    let list = [];

    if (!path) {
        list = products
    } else if (path === 'favorite') {
        list = products.filter((item, index) => favorite[item.articles])
    } else if (path === 'cart') {
        list = products.filter((item, index) => cart[item.articles] > 0)
    }

    return (
        <div className="App">
            <ProductList
                list={list}
                buttons={buttons}
                addToCart={addToCart}
                favorite={favorite}
                addToFavorite={addToFavorite}
                showModal={showModal}
                path={path}
            />
            <Modal setActive={setActive}
                   modals={modals}
                   displayModalId={displayModalId}
                   buttons={buttons}
                   addToCart={addToCart}
            />
            {/*<Btn {...buttons.btnCancel}/>*/}
            {/*<button onClick={() => this.showModal('first')} className="btnModal">Show first modal</button>*/}
            {/*<button onClick={() => this.showModal('second')} className="btnModal">Show second modal</button>*/}
        </div>
    );

}

export default InsteadApp;