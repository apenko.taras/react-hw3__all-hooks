import React from "react";
import {star} from "../../Icons/star";
import PropTypes from "prop-types";
import Product from "../Product/Product";

function FavoriteIcon ({index, favorite}) {

    if (favorite[index] === true) {
        return (
            star("gold", "prod__svg")
        )
    } else return star("grey", "prod__svg")
}

FavoriteIcon.propTypes = {
    index: PropTypes.number,
    favorite: PropTypes.object,
}

export default FavoriteIcon