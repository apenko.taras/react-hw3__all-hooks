import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Product from "../Product/Product";
import "./ProductList.scss"


class ProductList extends Component {

    renderProdCard = () => {
        const { list, buttons, addToCart, favorite, addToFavorite, showModal } = this.props;

        return list.map((product, index) => (
            <Product
                key={index}
                {...product}
                // buttons={buttons}
                // addToCart={addToCart}
                index={index}
                favorite={favorite}
                addToFavorite={addToFavorite}
                showModal={showModal}
            />
        ));
    }

    render() {
        return (
            <div className="products">{this.renderProdCard()}</div>
        )
    }
}

// ProductList.propTypes = {
//     list: PropTypes.arrayOf(
//         PropTypes.shape({
//             from: PropTypes.string,
//             to: PropTypes.string,
//             text: PropTypes.string
//         })
//     )
// };

export default ProductList;