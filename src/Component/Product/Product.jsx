import React from 'react';
import Btn from "../Btn/Btn";
import "./Product.scss"
import FavoriteIcon from "../Favorite/FavoriteIcon";
import PropTypes from "prop-types";

function Product({
                     name,
                     price,
                     urlPic,
                     index,
                     favorite,
                     addToFavorite,
                     showModal,
                     articles,
                     path,
                 }) {

    const handleClickBtn = () => {
        showModal('first', articles)
    }

    const handleClickRemoveCart = () => {
        showModal('second', articles)
    }

    const handleFavorite = () => {
        console.log('handleClickFavorite)))))))))))');
        addToFavorite(index)
    }

    const CartBtn = () => {
        if(path === 'cart'){
            return <Btn text="Remove from cart" backgroundColor="red" onClick={handleClickRemoveCart}/>
        } else {
            return <Btn text="Add to cart" backgroundColor="green" onClick={handleClickBtn}/>
        }
    }

    return (
        <div className="product-item">
            <div className="product-img"><img className="product-img__item" src={urlPic}/></div>
            <div className="product-list">
                <h3 className="product-name">{name}</h3>
                <span className="product-price">Price: {price}</span>
                <div className="product-buttons">
                    <div className="product-cart">
                        {/*<Btn text="Add to cart" backgroundColor="red" onClick={handleClickBtn}/>*/}
                        <CartBtn/>
                    </div>
                    <div className="product-btn__favorite" onClick={handleFavorite}>
                        {/*{star("grey", "prod__svg")}*/}
                        <FavoriteIcon index={index} favorite={favorite}/>
                    </div>
                </div>
            </div>
        </div>
    )

}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    urlPic: PropTypes.string,
    index: PropTypes.number,
    favorite: PropTypes.object,
}

export default Product;
