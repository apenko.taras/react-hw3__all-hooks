import React from 'react';
import AppRoutes from "./routes/AppRoutes";
import NavBar from "./Component/NavBar/NavBar";

const App = () => {
    return (
        <>
            <NavBar/>
            <AppRoutes/>
        </>
    );
};

export default App;
